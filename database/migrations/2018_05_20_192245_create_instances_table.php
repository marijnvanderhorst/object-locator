<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('label_id')->references('id')->on('labels');
            $table->integer('camera_id')->references('id')->on('cameras');
            $table->float('top_x', 9, 8);
            $table->float('top_y', 9, 8);
            $table->float('bottom_x', 9, 8);
            $table->float('bottom_y', 9, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instances');
    }
}
