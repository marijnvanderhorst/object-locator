Next update requires:

`composer install`

`php artisan migrate`

`php artisan passport:keys`

A new OAuth client can be created on your profile page, as well as a personal access token, which is very useful for testing purposes.
Or, via an artisan command:
`php artisan passport:client`.

An access token for this new client can be obtained using:
```PHP
$guzzle = new GuzzleHttp\Client;

$response = $guzzle->post('http://ol.pietervoors.com/oauth/token', [
    'form_params' => [
        'grant_type' => 'client_credentials',
        'client_id' => 'CLIENT_ID',
        'client_secret' => 'CLIENT_SECRET',
        'scope' => '*',
    ],
]);

return json_decode((string) $response->getBody(), true)['access_token'];
```

Uploading  an image for an instance:
`curl -X POST "ol.pietervoors.com/api/instance/INSTANCE_ID/image" -H "Accept: application/json" -H "Authorization: Bearer ACCESS_TOKEN_HERE" -F "img=@IMAGE_PATH"`
