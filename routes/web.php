<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('label', 'LabelController', [
        'only' => ['index', 'show']
    ]);
    Route::resource('instance', 'InstanceController', [
        'only' => ['show']
    ]);
    Route::post('user/favorites/set', 'FavoritesController@setFavorite')->name('favorites.set');
    Route::get('user/favorites', 'FavoritesController@show')->name('favorites.show');

    Route::get('user/profile', 'HomeController@profile')->name('profile');

    Route::get('storage/{folder}/{filename}', function ($folder, $filename)
    {
        $path = storage_path('app/'.$folder.'/'.$filename);

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });
});

/*
 * Auth routes. We don't use Auth::routes() as not all routes are desired.
 */
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::match(['get', 'post'], 'logout', 'Auth\LoginController@logout')->name('logout');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
