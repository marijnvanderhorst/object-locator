@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Items</h5>
                                        <p class="card-text">Show a list of items which have been located.</p>
                                        <a href="{{ route('label.index') }}" class="btn btn-primary">Items</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Favorites</h5>
                                        <p class="card-text">Show only items that have been added to favorites.</p>
                                        <a href="{{ route('favorites.show') }}" class="btn btn-primary">Favorites</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function (home, $, undefined) {

    }(window.home = window.home || {}, jQuery))

</script>
@endsection
