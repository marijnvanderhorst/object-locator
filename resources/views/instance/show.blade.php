@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('label.index') }}">Items</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('label.show', $instance->label) }}">{{ $instance->label->label }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $instance->label->label . ' ' . $instance->id }}</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">{{ $instance->label->label . ' ' . $instance->id }}
                        <i id="favorite" data-feather="star" stroke-width="1.5"
                                {!! Auth::user()->favorites->contains('id', $instance->id) ? 'fill="gold"' : '' !!}
                                data-toggle="tooltip" data-placement="top" title="Toggle favorites">
                        </i></div>

                    <div id="card" class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <h5 class="text-muted">Last seen {{ $instance->updated_at->diffForHumans() }}</h5>

                        <img id="image" src="{{ $instance->image_path }}" alt="Card image cap" hidden>
                        <canvas id="canvas" width="100%" height="100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        (function (instanceshow, $, undefined) {
            feather.replace();
            $('[data-toggle="tooltip"]').tooltip();

            $('#favorite').on('click', function (e) {
                let favorite;

                if ($(this).attr('fill') === 'gold') {
                    $(this).attr('fill', 'none');
                    favorite = false;
                } else {
                    $(this).attr('fill', 'gold');
                    favorite = true;
                }


                $.ajax({
                    url: '{{ route('favorites.set') }}',
                    method: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        favorite: +favorite,
                        instance_id: '{{ $instance->id }}',
                    },
                });

            });

            window.onload = function () {
                let canvas = document.getElementById('canvas');
                let cardWidth = $('#card').width()
                let ctx = canvas.getContext('2d');
                let img = document.getElementById('image');
                let outerBoundingBox = {!! json_encode($instance->bounding_box) !!}

                if (img.width > cardWidth) {
                    ctx.canvas.width = cardWidth;
                    ctx.canvas.height = img.height * (cardWidth / img.width);
                } else {
                    ctx.canvas.width = img.width;
                    ctx.canvas.height = img.height;
                }

                outerBoundingBox.width *= ctx.canvas.width;
                outerBoundingBox.height *= ctx.canvas.height;
                outerBoundingBox.x *= ctx.canvas.width;
                outerBoundingBox.x += outerBoundingBox.width / 2;
                outerBoundingBox.y *= ctx.canvas.height;
                outerBoundingBox.y += outerBoundingBox.height / 2;
                outerBoundingBox.lineWidth = 4;
                outerBoundingBox.strokeStyle = '#333E4B';
                outerBoundingBox.fillStyle = 'transparent';
                outerBoundingBox.anchor = 'center';

                let innerBoundingBox = _.clone(outerBoundingBox);
                outerBoundingBox.strokeStyle = '#FFFFFF';
                innerBoundingBox.lineWidth = 2;

                let image = new Facade.Image(img, {
                    x: 0,
                    y: 0,
                    width: img.width,
                    height: img.height,
                    scale: img.width > cardWidth ? (cardWidth / img.width) : 1,
                    anchor: 'top/left'
                });

                let outerRect = new Facade.Rect(outerBoundingBox);
                let innerRect = new Facade.Rect(innerBoundingBox);

                let stage = new Facade(document.querySelector('canvas')/*, ctx.canvas.width, ctx.canvas.height*/);
                let scale = 0;

                stage.draw(function () {
                    this.clear();

                    scale += 0.02;

                    if (scale > 1) {
                        scale = 1;
                    }

                    this.addToStage(image);
                    this.addToStage(outerRect, {scale: scale});
                    this.addToStage(innerRect, {scale: scale});
                });
            };
        }(window.instanceshow = window.instanceshow || {}, jQuery))

    </script>
@endsection
