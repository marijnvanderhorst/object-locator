<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoritesController extends Controller
{
    public function setFavorite(Request $request)
    {
        $request->validate([
            'favorite' => 'boolean',
            'instance_id' => 'exists:instances,id',
        ]);

        if ($request->get('favorite')) {
            Auth::user()->favorites()->attach($request->get('instance_id'));
        } else {
            Auth::user()->favorites()->detach($request->get('instance_id'));
        }
    }

    public function show()
    {
        return view('favorites');
    }
}
